﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SD.Cqrs;
using SD.Messaging.Models;
using Vostbank.Api.Data;
using Vostbank.Api.Extensions;
using Vostbank.Api.Models.Configuration;
using Vostbank.Api.Models.Inner;
using Vostbank.Api.Models.Vostbank;
using Vostbank.Api.Models.Vostbank.Enums;
using Yes.Contracts;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Commands;
using Yes.CreditApplication.Api.Contracts.CreditOrganizations;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.CreditApplication.Api.Contracts.Events;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Http;
using CreditApplicationRequest = Yes.CreditApplication.Api.Contracts.CreditOrganizations.CreditApplicationRequest;

namespace Vostbank.Api.Domain
{
    public interface ICreditApplicationManager
    {
        Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event);
        Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, ConfirmCreditApplicationRequest model);
    }

    public class CreditApplicationManager : ICreditApplicationManager
    {
        private readonly ILogger<CreditApplicationManager> logger;
        private readonly ICreditApplicationRepository repository;
        private readonly ICommandSender commandSender;
        private readonly IVostbankClient client;
        private readonly ApplicationConfiguration configuration;
        private readonly MessagingConfiguration messagingConfiguration;
        private const string SERVER_UNEXPECTED_ERROR_DETAILS = "Детали ошибки в логах приложения";
        
        public CreditApplicationManager(ILogger<CreditApplicationManager> logger, ICreditApplicationRepository repository, 
            ICommandSender commandSender, IVostbankClient client, ApplicationConfiguration configuration, 
            MessagingConfiguration messagingConfiguration)
        {
            this.logger = logger;
            this.repository = repository;
            this.commandSender = commandSender;
            this.client = client;
            this.configuration = configuration;
            this.messagingConfiguration = messagingConfiguration;
        }

        public async Task<ProcessingResult> CheckCreditApplication(CreditApplicationCreatedEvent @event)
        {
            var beginTime = DateTime.Now;
            try
            {
                if (@event.CreditAmount < configuration.MinCreditAmount)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.NotApplicable, @event.CreditApplicationId, $"Минимальная сумма кредита: {configuration.MinCreditAmount}");
                    logger.LogInformation($"CheckCreditApplication. Skip. Invalid credit amount. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }
                if (@event.ProfileType == ProfileType.Short)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.NotApplicable, @event.CreditApplicationId, "Заявка не отправляется для короткой анкеты");
                    logger.LogInformation($"CheckCreditApplication. Skip. Unsupported profile type. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }
                
                var request = createRequest(@event);
                
                var response = await client.CreateApplication(request);
                await repository.SaveCreditApplicationCreateRequest(new CreditApplicationCreateModel
                {
                    CreditApplicationId = @event.CreditApplicationId,
                    CreatedDate = beginTime,
                    HttpStatusCode = response.StatusCode,
                    ErrorMessage = response.ErrorMessage,
                    VostbankUuid = response.Content?.uuid,
                    VostbankId = response.Content?.id,
                    VostbankPcid = response.Content?.pcid,
                    VostbankDouble = response.Content?.@double,
                    VostbankStatus = response.Content?.status,
                    VostbankAddDate = response.Content?.add_date,
                    VostbankSubmitDate = response.Content?.submit_date,
                    VostbankSummFact = response.Content?.summ_fact,
                    VostbankDateFact = response.Content?.date_fact
                });
                
                if (response.IsSuccessStatusCode)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    // ReSharper disable once PossibleInvalidOperationException
                    var confirmResponse = await client.ConfirmApplication(response.Content.id, response.Content.uuid.Value);
                    await repository.SaveCreditApplicationConfirmRequest(new CreditApplicationCreateModel
                    {
                        CreditApplicationId = @event.CreditApplicationId,
                        CreatedDate = beginTime,
                        HttpStatusCode = confirmResponse.StatusCode,
                        ErrorMessage = confirmResponse.ErrorMessage,
                        VostbankUuid = confirmResponse.Content?.uuid,
                        VostbankId = confirmResponse.Content?.id,
                        VostbankPcid = confirmResponse.Content?.pcid,
                        VostbankDouble = confirmResponse.Content?.@double,
                        VostbankStatus = confirmResponse.Content?.status,
                        VostbankAddDate = confirmResponse.Content?.add_date,
                        VostbankSubmitDate = confirmResponse.Content?.submit_date,
                        VostbankSummFact = confirmResponse.Content?.summ_fact,
                        VostbankDateFact = confirmResponse.Content?.date_fact
                    });
                    
                    if (confirmResponse.IsSuccessStatusCode)
                    {
                        var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.Confirmed, @event.CreditApplicationId);
                        logger.LogInformation($"CheckCreditApplication. Confirm credit application request success. Duration: {beginTime.GetDuration()} CreateRequest: {request}, CreateResponse: {response.Content}, ConfirmResponse: {confirmResponse.Content}, Command: {command}");
                        return ProcessingResult.Ok();
                    }
                    
                    if (confirmResponse.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ValidationErrorConfirm, @event.CreditApplicationId, confirmResponse.ErrorMessage);
                        logger.LogInformation($"CheckCreditApplication. Confirm credit application request validation fail (StatusCode = {confirmResponse.StatusCode} {(int)confirmResponse.StatusCode}). Duration: {beginTime.GetDuration()} CreateRequest: {request}, CreateResponse: {response.Content}, ErrorMessage: {confirmResponse.ErrorMessage}, Command: {command}");
                        return ProcessingResult.Ok();
                    }

                    if (confirmResponse.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorConfirm, @event.CreditApplicationId, confirmResponse.ErrorMessage);
                        logger.LogError($"CheckCreditApplication. Confirm credit application request authorization fail (StatusCode = {confirmResponse.StatusCode} {(int)confirmResponse.StatusCode}). Duration: {beginTime.GetDuration()} CreateRequest: {request}, CreateResponse: {response.Content}, ErrorMessage: {confirmResponse.ErrorMessage}, Command: {command}");
                        return ProcessingResult.Ok();
                    }

                    if (@event.RetryCount == messagingConfiguration.MaxRetryCount - 1)
                    {
                        var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, @event.CreditApplicationId,
                            $"Количество отправленных запросов: {messagingConfiguration.MaxRetryCount}. Последняя ошибка: {confirmResponse.ErrorMessage}");
                        logger.LogWarning($"CheckCreditApplication. Confirm credit application request fail (StatusCode = {confirmResponse.StatusCode} {(int)confirmResponse.StatusCode}). Max retry count ({messagingConfiguration.MaxRetryCount}) exceeded, finish processing. Duration: {beginTime.GetDuration()} CreateRequest: {request}, CreateResponse: {response.Content}, ErrorMessage: {confirmResponse.ErrorMessage}, Command: {command}");
                        return ProcessingResult.Ok();
                    }

                    // Переповтор
                    logger.LogWarning($"CheckCreditApplication. Confirm credit application request fail (StatusCode = {confirmResponse.StatusCode} {(int)confirmResponse.StatusCode}), try again later. Duration: {beginTime.GetDuration()} CreateRequest: {request}, CreateResponse: {response.Content}, ErrorMessage: {confirmResponse.ErrorMessage}");
                    return ProcessingResult.Fail();
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ValidationErrorCheck, @event.CreditApplicationId, response.ErrorMessage);
                    logger.LogInformation($"CheckCreditApplication. Create credit application request validation fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }
                
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorCheck, @event.CreditApplicationId, response.ErrorMessage);
                    logger.LogError($"CheckCreditApplication. Create credit application request authorization fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }
               
                if (@event.RetryCount == messagingConfiguration.MaxRetryCount - 1)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorCheck, @event.CreditApplicationId,
                        $"Количество отправленных запросов: {messagingConfiguration.MaxRetryCount}. Последняя ошибка: {response.ErrorMessage}");
                    logger.LogWarning($"CheckCreditApplication. Create credit application request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Max retry count ({messagingConfiguration.MaxRetryCount}) exceeded, finish processing. Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                // Переповтор
                logger.LogWarning($"CheckCreditApplication. Create credit application request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}), try again later. Duration: {beginTime.GetDuration()} CreditApplicationId: {@event.CreditApplicationId}, Request: {request}, ErrorMessage: {response.ErrorMessage}");
                return ProcessingResult.Fail();
            }
            catch (Exception e)
            {
                if (@event.RetryCount == messagingConfiguration.MaxRetryCount - 1)
                {
                    var command = publishDecisionCommand(beginTime, CreditOrganizationRequestStatus.ServerErrorCheck, @event.CreditApplicationId,
                        $"Количество попыток обработки: {messagingConfiguration.MaxRetryCount}. {SERVER_UNEXPECTED_ERROR_DETAILS}");
                    logger.LogError(e, $"CheckCreditApplication. Error while processing credit application request. Max retry count ({messagingConfiguration.MaxRetryCount}) exceeded, finish processing. Duration: {beginTime.GetDuration()} Event: {@event}, Command: {command}");
                    return ProcessingResult.Ok();
                }

                logger.LogError(e, $"CheckCreditApplication. Error while processing credit application request, try again later. Duration: {beginTime.GetDuration()} Event: {@event}");
                return ProcessingResult.Fail();
            }
        }

        public async Task<Response<CreditApplicationResponse>> ConfirmCreditApplication(Guid creditApplicationId, ConfirmCreditApplicationRequest model)
        {
            var beginTime = DateTime.Now;
            try
            {
                if (model.CreditAmount < configuration.MinCreditAmount)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Skip. Invalid credit amount. Duration: {beginTime.GetDuration()} Model: {model}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.NotApplicable, $"Минимальная сумма кредита: {configuration.MinCreditAmount}");
                }
                if (model.ProfileType == ProfileType.Short)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Skip. Unsupported profile type. Duration: {beginTime.GetDuration()} Model: {model}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.NotApplicable, "Заявка не отправляется для короткой анкеты");
                }

                var vostbankModel = await repository.GetCreditApplicationUuid(creditApplicationId);
                var request = createRequest(model);
                Response<ApplicationResponse> response;
                Guid? vostbankUuid;
                if (vostbankModel != null)
                {
                    response = await client.UpdateApplication(request, vostbankModel.VostbankId, vostbankModel.VostbankUuid);
                    await repository.SaveCreditApplicationUpdateRequest(new CreditApplicationCreateModel
                    {
                        CreditApplicationId = creditApplicationId,
                        CreatedDate = beginTime,
                        HttpStatusCode = response.StatusCode,
                        ErrorMessage = response.ErrorMessage,
                        VostbankUuid = response.Content?.uuid,
                        VostbankId = response.Content?.id,
                        VostbankPcid = response.Content?.pcid,
                        VostbankDouble = response.Content?.@double,
                        VostbankStatus = response.Content?.status,
                        VostbankAddDate = response.Content?.add_date,
                        VostbankSubmitDate = response.Content?.submit_date,
                        VostbankSummFact = response.Content?.summ_fact,
                        VostbankDateFact = response.Content?.date_fact
                    });
                    vostbankUuid = vostbankModel.VostbankUuid;
                }
                else
                {
                    response = await client.CreateApplication(request);
                    await repository.SaveCreditApplicationCreateRequest(new CreditApplicationCreateModel
                    {
                        CreditApplicationId = creditApplicationId,
                        CreatedDate = beginTime,
                        HttpStatusCode = response.StatusCode,
                        ErrorMessage = response.ErrorMessage,
                        VostbankUuid = response.Content?.uuid,
                        VostbankId = response.Content?.id,
                        VostbankPcid = response.Content?.pcid,
                        VostbankDouble = response.Content?.@double,
                        VostbankStatus = response.Content?.status,
                        VostbankAddDate = response.Content?.add_date,
                        VostbankSubmitDate = response.Content?.submit_date,
                        VostbankSummFact = response.Content?.summ_fact,
                        VostbankDateFact = response.Content?.date_fact
                    });
                    vostbankUuid = response.Content?.uuid;
                }
                
                if (response.IsSuccessStatusCode)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    // ReSharper disable once PossibleInvalidOperationException
                    var confirmResponse = await client.ConfirmApplication(response.Content.id, vostbankUuid.Value);
                    await repository.SaveCreditApplicationConfirmRequest(new CreditApplicationCreateModel
                    {
                        CreditApplicationId = creditApplicationId,
                        CreatedDate = beginTime,
                        HttpStatusCode = confirmResponse.StatusCode,
                        ErrorMessage = confirmResponse.ErrorMessage,
                        VostbankUuid = confirmResponse.Content?.uuid,
                        VostbankId = confirmResponse.Content?.id,
                        VostbankPcid = confirmResponse.Content?.pcid,
                        VostbankDouble = confirmResponse.Content?.@double,
                        VostbankStatus = confirmResponse.Content?.status,
                        VostbankAddDate = confirmResponse.Content?.add_date,
                        VostbankSubmitDate = confirmResponse.Content?.submit_date,
                        VostbankSummFact = confirmResponse.Content?.summ_fact,
                        VostbankDateFact = confirmResponse.Content?.date_fact
                    });
                    
                    if (confirmResponse.IsSuccessStatusCode)
                    {
                        logger.LogInformation($"ConfirmCreditApplication. Confirm credit application request success. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreateRequest: {request}, CreateResponse: {response.Content}, ConfirmResponse: {confirmResponse.Content}");
                        return createResponse(beginTime, CreditOrganizationRequestStatus.Confirmed);
                    }
                    
                    if (confirmResponse.StatusCode == HttpStatusCode.BadRequest)
                    {
                        logger.LogInformation($"ConfirmCreditApplication. Confirm credit application request validation fail (StatusCode = {confirmResponse.StatusCode} {(int)confirmResponse.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreateRequest: {request}, CreateResponse: {response.Content}, ErrorMessage: {confirmResponse.ErrorMessage}");
                        return createResponse(beginTime, CreditOrganizationRequestStatus.ValidationErrorConfirm, confirmResponse.ErrorMessage);
                    }

                    if (confirmResponse.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        logger.LogError($"ConfirmCreditApplication. Confirm credit application request authorization fail (StatusCode = {confirmResponse.StatusCode} {(int)confirmResponse.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreateRequest: {request}, CreateResponse: {response.Content}, ErrorMessage: {confirmResponse.ErrorMessage}");
                        return createResponse(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorConfirm, confirmResponse.ErrorMessage);
                    }

                    logger.LogWarning($"ConfirmCreditApplication. Confirm credit application request fail (StatusCode = {confirmResponse.StatusCode} {(int)confirmResponse.StatusCode}), try again later. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreateRequest: {request}, CreateResponse: {response.Content}, ErrorMessage: {confirmResponse.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, confirmResponse.ErrorMessage);
                }

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    logger.LogInformation($"ConfirmCreditApplication. Create credit application request validation fail (StatusCode = {response.StatusCode} {(int) response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.ValidationErrorConfirm, response.ErrorMessage);
                }

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    logger.LogError($"ConfirmCreditApplication. Create credit application request authorization fail (StatusCode = {response.StatusCode} {(int) response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, ErrorMessage: {response.ErrorMessage}");
                    return createResponse(beginTime, CreditOrganizationRequestStatus.AuthorizaionErrorConfirm, response.ErrorMessage);
                }

                logger.LogWarning($"ConfirmCreditApplication. Create credit application request fail (StatusCode = {response.StatusCode} {(int)response.StatusCode}). Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {request.ToJsonString()}, ErrorMessage: {response.ErrorMessage}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, response.ErrorMessage);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"ConfirmCreditApplication. Error while processing credit application request. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Model: {model}");
                return createResponse(beginTime, CreditOrganizationRequestStatus.ServerErrorConfirm, SERVER_UNEXPECTED_ERROR_DETAILS);
            }
        }

        private ApplicationRequest createRequest(CreditApplicationRequest request)
        {
            return new ApplicationRequest
            {
                utm_medium = "affiliate_cash",
                utm_source = "yesfs",
                utm_campaign = request.PartnerId.ToString(),
                period = request.CreditPeriod / 30 ?? 0,
                summa = request.CreditAmount ?? 0,
                lastname = request.LastName,
                firstname = request.FirstName,
                middlename = request.MiddleName,
                mobile = request.PhoneNumber.ToVostbankPhoneFormat(),
                email = request.Email,
                gender = request.Gender.HasValue ? (request.Gender == Gender.Male ? VostbankGender.Male : VostbankGender.Female).GetDescription() : null,
                birthdate = request.DateOfBirth.ToVostbankDateFormat(),
                pnumber = $"{request.PassportSeries} {request.PassportNumber}",
                pdate = request.PassportIssueDate.ToVostbankDateFormat(),
                porgname = request.PassportIssuer,
                porgcode = request.PassportDepartmentCode,
                registration_address = new Address
                {
                    city = request.RegistrationAddressCityKladrCode,
                    street = request.RegistrationAddressStreetKladrCode,
                    house = request.RegistrationAddressHouse,
                    lit = request.RegistrationAddressBlock,
                    flat = request.RegistrationAddressApartment
                },
                address = new Address
                {
                    city = request.ResidenceAddressCityKladrCode,
                    street = request.ResidenceAddressStreetKladrCode,
                    house = request.ResidenceAddressHouse,
                    lit = request.ResidenceAddressBlock,
                    flat = request.ResidenceAddressApartment
                },
                flagBki = request.CreditBureauProcessApproved,
                flagBkiDate = request.CreditBureauProcessApproveDate.ToString("o"),
                category = VostbankCategory.Credit.GetDescription()
            };
        }

        private CreditApplicationDecisionsCommand publishDecisionCommand(DateTime date, CreditOrganizationRequestStatus status, Guid creditApplicationId, string details = null)
        {
            var command = new CreditApplicationDecisionsCommand
            {
                Date = date,
                CreditApplicationId = creditApplicationId,
                CreditApplications = new List<CreditApplicationStatusModel>
                {
                    new CreditApplicationStatusModel
                    {
                        CreditOrganizationId = configuration.CreditOrganizationId,
                        Status = status,
                        Details = details
                    }
                }
            };
            commandSender.SendCommand(command, BoundedContexts.CREDIT_APPLICATION_API, Routes.Decisions);
            return command;
        }
        
        private Response<CreditApplicationResponse> createResponse(DateTime date, CreditOrganizationRequestStatus status, string details = null)
        {
            return Response<CreditApplicationResponse>.Ok(new CreditApplicationResponse
            {
                Date = date,
                CreditApplications = new List<CreditApplicationStatusModel>
                {
                    new CreditApplicationStatusModel
                    {
                        CreditOrganizationId = configuration.CreditOrganizationId,
                        Status = status,
                        Details = details
                    }
                }
            });
        }
    }
}