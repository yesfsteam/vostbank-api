﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Vostbank.Api.Models.Configuration;
using Vostbank.Api.Models.Vostbank;
using Yes.Infrastructure.Http;

namespace Vostbank.Api.Domain
{
    public interface IVostbankClient
    {
        Task<Response<ApplicationResponse>> CreateApplication(ApplicationRequest request);
        Task<Response<ApplicationResponse>> UpdateApplication(ApplicationRequest request, int id, Guid uuid);
        Task<Response<ApplicationResponse>> ConfirmApplication(int id, Guid uuid);
    }

    public class VostbankClient : IVostbankClient
    {
        private volatile HttpClient httpClient;
        private readonly VostbankConfiguration configuration;
        
        public VostbankClient(HttpClient httpClient, VostbankConfiguration configuration)
        {
            this.httpClient = httpClient;
            this.configuration = configuration;
        }

        public async Task<Response<ApplicationResponse>> CreateApplication(ApplicationRequest request)
        {
            return await post<ApplicationResponse>($"v1/request/credit?api_token={configuration.ApiToken}", request);
        }

        public async Task<Response<ApplicationResponse>> UpdateApplication(ApplicationRequest request, int id, Guid uuid)
        {
            return await put<ApplicationResponse>($"v1/request/credit/{id}?uuid={uuid}&api_token={configuration.ApiToken}", request);
        }

        public async Task<Response<ApplicationResponse>> ConfirmApplication(int id, Guid uuid)
        {
            return await post<ApplicationResponse>($"v1/request/credit/{id}/activate?uuid={uuid}&api_token={configuration.ApiToken}");
        }
        
        private async Task<Response<T>> post<T>(string uri, object obj)
        {
            var requestUri = new Uri(httpClient.BaseAddress, uri);
            var content = new JsonContent(obj, new JsonSerializerSettings());
            var response = await httpClient.PostAsync(requestUri, content);
            return await createResponse<T>(response);
        }
        
        private async Task<Response<T>> put<T>(string uri, object obj)
        {
            var requestUri = new Uri(httpClient.BaseAddress, uri);
            var content = new JsonContent(obj, new JsonSerializerSettings());
            var response = await httpClient.PutAsync(requestUri, content);
            return await createResponse<T>(response);
        }
        
        private async Task<Response<T>> post<T>(string uri)
        {
            var requestUri = new Uri(httpClient.BaseAddress, uri);
            var response = await httpClient.PostAsync(requestUri, null);
            return await createResponse<T>(response);
        }
        
        private async Task<Response<T>> createResponse<T>(HttpResponseMessage response)
        {
            var result = new Response<T>
            {
                IsSuccessStatusCode = response.IsSuccessStatusCode,
                StatusCode = response.StatusCode
            };
            
            var content = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
                result.Content = JsonConvert.DeserializeObject<T>(content);
            else
                result.ErrorMessage = content;
            
            return result;
        }
    }
}