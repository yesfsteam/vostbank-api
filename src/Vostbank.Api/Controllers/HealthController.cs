﻿using Microsoft.AspNetCore.Mvc;

namespace Vostbank.Api.Controllers
{
    [Route("[controller]")]
    public class HealthController : ControllerBase
    {
        /// <summary>
        /// Проверка работоспособности приложения
        /// </summary>
        [HttpGet]
        public IActionResult Index()
        {
            return Ok("Ok");
        }
    }
}