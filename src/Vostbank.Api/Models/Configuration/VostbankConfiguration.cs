﻿namespace Vostbank.Api.Models.Configuration
{
    public class VostbankConfiguration
    {
        public string ApiToken { get; set; }
    }
}