﻿using System;

namespace Vostbank.Api.Models.Configuration
{
    public class ApplicationConfiguration
    {
        public Guid CreditOrganizationId { get; set; }
        public int MinCreditAmount { get; set; }
    }
}