﻿using Yes.Infrastructure.Common.Models;
// ReSharper disable InconsistentNaming

namespace Vostbank.Api.Models.Vostbank
{
    public class Address : JsonModel
    {
        public string city { get; set; }
        public string street { get; set; }
        public string house { get; set; }
        public string lit { get; set; }
        public string flat { get; set; }
        //public string telephone { get; set; }
    }
}