﻿using System.ComponentModel;

namespace Vostbank.Api.Models.Vostbank.Enums
{
    public enum VostbankCategory
    {
        [Description("credit")]
        Credit = 1,
        [Description("Kpz")]
        Kpz = 2
    }
}