﻿using System.ComponentModel;
// ReSharper disable InconsistentNaming

namespace Vostbank.Api.Models.Vostbank.Enums
{
    public enum VostbankStatus
    {
        [Description("Заявка принята")]
        Accepted = 1,
        [Description("Отказ")]
        Refusal = 2,
        [Description("Лид в КЦ")]
        LeadInCC = 4,
        [Description("Одобрено")]
        Approved = 5,
        [Description("Выдано КН")]
        IssuedKN = 6,
        [Description("Выдано КК")]
        IssuedKK = 7
    }
}