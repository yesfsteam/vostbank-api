﻿using System.ComponentModel;

namespace Vostbank.Api.Models.Vostbank.Enums
{
    public enum VostbankGender
    {
        [Description("male")]
        Male = 1,
        [Description("female")]
        Female = 2
    }
}