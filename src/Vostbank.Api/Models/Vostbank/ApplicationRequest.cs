﻿using System;
using Yes.Infrastructure.Common.Models;
// ReSharper disable InconsistentNaming

namespace Vostbank.Api.Models.Vostbank
{
    public class ApplicationRequest : JsonModel
    {
        public Guid? uuid { get; set; }
        //public int uid { get; set; }
        //public string ipaddress { get; set; }
        //public string httpreferer { get; set; }
        //public string httpagent { get; set; }
        //public string cid { get; set; }
        //public string google_utmz { get; set; }
        public string utm_medium { get; set; }
        public string utm_source { get; set; }
        public string utm_campaign { get; set; }
        //public string utm_content { get; set; }
        //public string utm_term { get; set; }
        public int period { get; set; }
        public int summa { get; set; }
        public string lastname { get; set; }
        public string firstname { get; set; }
        public string middlename { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string gender { get; set; }
        public string birthdate { get; set; }
        //public string lastname_old { get; set; }
        //public string firstname_old { get; set; }
        //public string middlename_old { get; set; }
        //public string bcity { get; set; }
        //public string bcity_custom_country { get; set; }
        //public string bcity_custom_region { get; set; }
        //public string bcity_custom_district { get; set; }
        //public string bcity_custom_city { get; set; }
        public string pnumber { get; set; }
        public string pdate { get; set; }
        public string porgname { get; set; }
        public string porgcode { get; set; }
        //public string snils { get; set; }
        //public string pnumber_old { get; set; }
        //public string pdate_old { get; set; }
        //public string porgcode_old { get; set; }
        public Address registration_address { get; set; }
        //public Address temporary_registration_address { get; set; }
        public Address address { get; set; }
        public bool flagBki { get; set; }
        public string flagBkiDate { get; set; }
        public string category { get; set; }
    }
}