﻿// ReSharper disable InconsistentNaming
using System;
using Vostbank.Api.Models.Vostbank.Enums;

namespace Vostbank.Api.Models.Vostbank
{
    public class ApplicationResponse : ApplicationRequest
    {
        public int id { get; set; }
        public int? pcid { get; set; }
        public byte @double { get; set; }
        public VostbankStatus? status { get; set; }
        public DateTime add_date { get; set; }
        public DateTime? submit_date { get; set; }
        public int? summ_fact { get; set; }
        public DateTime? date_fact { get; set; }
    }
}