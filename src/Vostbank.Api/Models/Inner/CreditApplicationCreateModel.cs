﻿using System;
using System.Net;
using Vostbank.Api.Models.Vostbank.Enums;
using Yes.Infrastructure.Common.Models;

namespace Vostbank.Api.Models.Inner
{
    public class CreditApplicationCreateModel : JsonModel
    {
        /// <summary>
        /// Идентификатор заявки на кредит
        /// </summary>
        public Guid CreditApplicationId { get; set; }
        
        /// <summary>
        /// Дата решения по заявке на кредит
        /// </summary>
        public DateTime CreatedDate { get; set; }
        
        /// <summary>
        /// Http статус запроса
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }
        
        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorMessage { get; set; }
        
        /// <summary>
        /// Идентификатор заявки Банка Восточный
        /// </summary>
        public Guid? VostbankUuid { get; set; }
        
        /// <summary>
        /// Внутренний id заявки в БД сайта
        /// </summary>
        public int? VostbankId { get; set; }
        
        /// <summary>
        /// Id заявки в ПКК
        /// </summary>
        public int? VostbankPcid { get; set; }
        
        /// <summary>
        /// Является ли заявка дублем уже существующей
        /// </summary>
        public byte? VostbankDouble { get; set; }
        
        /// <summary>
        /// Статус заявки
        /// </summary>
        public VostbankStatus? VostbankStatus { get; set; }
        
        /// <summary>
        /// Дата создания заявки
        /// </summary>
        public DateTime? VostbankAddDate { get; set; }
        
        /// <summary>
        /// Дата отправки заявки в ПКК
        /// </summary>
        public DateTime? VostbankSubmitDate { get; set; }
        
        /// <summary>
        /// Сумма выданного кредита
        /// </summary>
        public int? VostbankSummFact { get; set; }
        
        /// <summary>
        /// Дата выдачи кредита
        /// </summary>
        public DateTime? VostbankDateFact { get; set; }
    }
}