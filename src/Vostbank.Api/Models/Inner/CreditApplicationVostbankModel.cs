﻿using System;
using Yes.Infrastructure.Common.Models;

namespace Vostbank.Api.Models.Inner
{
    public class CreditApplicationVostbankModel : JsonModel
    {
        /// <summary>
        /// Идентификатор заявки Банка Восточный
        /// </summary>
        public Guid VostbankUuid { get; set; }
        
        /// <summary>
        /// Внутренний id заявки в БД сайта
        /// </summary>
        public int VostbankId { get; set; }
    }
}