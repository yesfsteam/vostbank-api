﻿using System;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Vostbank.Api.Models.Inner;

namespace Vostbank.Api.Data
{
    public interface ICreditApplicationRepository
    {
        Task SaveCreditApplicationCreateRequest(CreditApplicationCreateModel model);
        Task SaveCreditApplicationUpdateRequest(CreditApplicationCreateModel model);
        Task SaveCreditApplicationConfirmRequest(CreditApplicationCreateModel model);
        Task<CreditApplicationVostbankModel> GetCreditApplicationUuid(Guid creditApplicationId);
    }

    public class CreditApplicationRepository : ICreditApplicationRepository
    {
        private readonly IConfiguration configuration;

        public CreditApplicationRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task SaveCreditApplicationCreateRequest(CreditApplicationCreateModel model)
        {
            var queryParams = new
            {
                model.CreditApplicationId,
                model.CreatedDate,
                model.HttpStatusCode,
                model.ErrorMessage,
                model.VostbankUuid,
                model.VostbankId,
                model.VostbankPcid,
                model.VostbankDouble,
                model.VostbankStatus,
                model.VostbankAddDate,
                model.VostbankSubmitDate,
                model.VostbankSummFact,
                model.VostbankDateFact
            };

            await using var connection = createConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO credit_application_create_request (
                    credit_application_id,
                    created_date,
                    http_status_code,
                    error_message,
                    vostbank_uuid,
                    vostbank_id,
                    vostbank_pcid,
                    vostbank_double,
                    vostbank_status,
                    vostbank_add_date,
                    vostbank_submit_date,
                    vostbank_summ_fact,
                    vostbank_date_fact
                ) VALUES (
                    :creditApplicationId::uuid,
                    :createdDate,
                    :httpStatusCode,
                    :errorMessage,
                    :vostbankUuid,
                    :vostbankId,
                    :vostbankPcid,
                    :vostbankDouble,
                    :vostbankStatus,
                    :vostbankAddDate,
                    :vostbankSubmitDate,
                    :vostbankSummFact,
                    :vostbankDateFact
                );", queryParams);
        }
        
        public async Task SaveCreditApplicationUpdateRequest(CreditApplicationCreateModel model)
        {
            var queryParams = new
            {
                model.CreditApplicationId,
                model.CreatedDate,
                model.HttpStatusCode,
                model.ErrorMessage,
                model.VostbankUuid,
                model.VostbankId,
                model.VostbankPcid,
                model.VostbankDouble,
                model.VostbankStatus,
                model.VostbankAddDate,
                model.VostbankSubmitDate,
                model.VostbankSummFact,
                model.VostbankDateFact
            };

            await using var connection = createConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO credit_application_update_request (
                    credit_application_id,
                    created_date,
                    http_status_code,
                    error_message,
                    vostbank_uuid,
                    vostbank_id,
                    vostbank_pcid,
                    vostbank_double,
                    vostbank_status,
                    vostbank_add_date,
                    vostbank_submit_date,
                    vostbank_summ_fact,
                    vostbank_date_fact
                ) VALUES (
                    :creditApplicationId::uuid,
                    :createdDate,
                    :httpStatusCode,
                    :errorMessage,
                    :vostbankUuid,
                    :vostbankId,
                    :vostbankPcid,
                    :vostbankDouble,
                    :vostbankStatus,
                    :vostbankAddDate,
                    :vostbankSubmitDate,
                    :vostbankSummFact,
                    :vostbankDateFact
                );", queryParams);
        }
        
        public async Task SaveCreditApplicationConfirmRequest(CreditApplicationCreateModel model)
        {
            var queryParams = new
            {
                model.CreditApplicationId,
                model.CreatedDate,
                model.HttpStatusCode,
                model.ErrorMessage,
                model.VostbankUuid,
                model.VostbankId,
                model.VostbankPcid,
                model.VostbankDouble,
                model.VostbankStatus,
                model.VostbankAddDate,
                model.VostbankSubmitDate,
                model.VostbankSummFact,
                model.VostbankDateFact
            };

            await using var connection = createConnection();
            await connection.ExecuteAsync(@"
                INSERT INTO credit_application_confirm_request (
                    credit_application_id,
                    created_date,
                    http_status_code,
                    error_message,
                    vostbank_uuid,
                    vostbank_id,
                    vostbank_pcid,
                    vostbank_double,
                    vostbank_status,
                    vostbank_add_date,
                    vostbank_submit_date,
                    vostbank_summ_fact,
                    vostbank_date_fact
                ) VALUES (
                    :creditApplicationId::uuid,
                    :createdDate,
                    :httpStatusCode,
                    :errorMessage,
                    :vostbankUuid,
                    :vostbankId,
                    :vostbankPcid,
                    :vostbankDouble,
                    :vostbankStatus,
                    :vostbankAddDate,
                    :vostbankSubmitDate,
                    :vostbankSummFact,
                    :vostbankDateFact
                );", queryParams);
        }

        public async Task<CreditApplicationVostbankModel> GetCreditApplicationUuid(Guid creditApplicationId)
        {
            await using var connection = createConnection();
            return await connection.QueryFirstOrDefaultAsync<CreditApplicationVostbankModel>(@"
                SELECT
                    vostbank_uuid,
                    vostbank_id
                FROM credit_application_create_request
                WHERE
                    credit_application_id = :creditApplicationId::uuid
                    AND http_status_code = 200
                ORDER BY
                    created_date DESC
                limit 1;", new {creditApplicationId});
        }

        private NpgsqlConnection createConnection()
        {
            return new NpgsqlConnection(configuration.GetConnectionString("Database"));
        }
    }
}