﻿namespace Vostbank.Api.Extensions
{
    public static class StringExtensions
    {
        public static string ToVostbankPhoneFormat(this string phone)
        {
            return string.IsNullOrWhiteSpace(phone) ? null : phone.Substring(1);
        }
    }
}