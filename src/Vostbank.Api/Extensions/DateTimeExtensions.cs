﻿using System;

namespace Vostbank.Api.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToVostbankDateFormat(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToString("yyyy-MM-dd") : string.Empty;
        }
    }
}