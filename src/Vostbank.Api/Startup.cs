using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using SD.Cqrs;
using SD.Cqrs.NetCore;
using SD.Logger.Serilog.NetCore;
using SD.Messaging.RabbitMQ.NetCore;
using Serilog;
using Vostbank.Api.Data;
using Vostbank.Api.Domain;
using Vostbank.Api.Messaging;
using Vostbank.Api.Models.Configuration;
using Yes.Contracts;
using Yes.CreditApplication.Api.Contracts;
using Yes.CreditApplication.Api.Contracts.Commands;
using Yes.CreditApplication.Api.Contracts.Events;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Http.Extensions;

namespace Vostbank.Api
{
    public class Startup
    {
	    private readonly string applicationName;
	    private readonly IConfiguration configuration;
	    
	    public Startup(IConfiguration configuration, IHostEnvironment env)
        {
            this.configuration = configuration;
            applicationName = env.ApplicationName;
        }

        public void ConfigureServices(IServiceCollection services)
        {
	        var messagingConfiguration = configuration.BindFromAppConfig<MessagingConfiguration>();
	        services.AddSingleton(messagingConfiguration);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddRouting(options => options.LowercaseUrls = true);
            
            services.AddCqrsEngine()
	            .WithSerilogLogger()
	            .WithRabbitMQMessagingEngine()
	            .WithBoundedContext(BoundedContext.Create(BoundedContexts.VOSTBANK_API)
		            .ListeningEvents(typeof(CreditApplicationCreatedEvent)).From(BoundedContexts.CREDIT_APPLICATION_API).WithMaxRetry(messagingConfiguration.MaxRetryCount)
		            .PublishingCommands(typeof(CreditApplicationDecisionsCommand)).To(BoundedContexts.CREDIT_APPLICATION_API).On(Routes.Decisions)
		            .Build())
	            .AddProjection<CreditApplicationProjection>(BoundedContexts.CREDIT_APPLICATION_API, messagingConfiguration.ChannelsCount);
            
            services.AddTransient<ICreditApplicationRepository, CreditApplicationRepository>();
            services.AddTransient<ICreditApplicationManager, CreditApplicationManager>();

            services.AddSingleton(configuration.BindFromAppConfig<VostbankConfiguration>());
            services.AddSingleton(configuration.BindFromAppConfig<ApplicationConfiguration>());
            services.AddHttpClientFromConfiguration<IVostbankClient, VostbankClient>(configuration, "VostbankConfiguration");

            
            if (configuration.GetValue<bool>("EnableSwagger"))
	            services.AddSwaggerGen(c =>
	            {
		            c.SwaggerDoc("v1", new OpenApiInfo { Title = applicationName, Version = "v1" });
		            c.IncludeXmlComments(Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, $"{applicationName}.xml"));
	            });


			Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();

            DapperInitializer.ConfigureDapper();
		}

        public void Configure(IApplicationBuilder app, IHostApplicationLifetime applicationLifetime, CqrsEngine cqrsEngine)
        {
	        applicationLifetime.ApplicationStopping.Register(()=>onApplicationStopping(cqrsEngine));

	        app.UseRouting();
	        app.UseEndpoints(endpoints => endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}"));

	        if (configuration.GetValue<bool>("EnableSwagger"))
	        {
		        app.UseSwagger();
		        app.UseSwaggerUI(c =>
		        {
			        c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{applicationName} v1");
			        c.RoutePrefix = string.Empty;
		        });
	        }
            
	        cqrsEngine.Start();
            
	        Log.Logger.Information($"{applicationName} has been started");
        }

        private void onApplicationStopping(CqrsEngine cqrsEngine)
        {
	        cqrsEngine.Stop();
	        Log.Logger.Information($"{applicationName} has been stopped");
        }
	}
}